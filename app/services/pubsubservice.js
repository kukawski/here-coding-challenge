define([
	"./module",
], function (services) {
	return services.factory("pubsub", ["$rootScope", function ($rootScope) {
		return {
			publish: function (topic, data) {
				$rootScope.$broadcast(topic, data);
			},

			subscribe: function (topic, scope, func) {
				scope.$on(topic, func);
			}/*,

			unsubscribe: function (topic, scope, func) {
				// todo
			}*/
		}
	}]);
});