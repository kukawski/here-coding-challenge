define([
	"./app",
	"ngRoute"
], function (app) {
	app.config(["$routeProvider", function ($routeProvider) {
		$routeProvider.when("/", {
			templateUrl: "app/partials/route-creator.html",
			controller: "RouteCreatorController"
		});

		$routeProvider.otherwise({
			redirectTo: "/"
		});
	}]);
});