define([
	"./module",
	"../services/routingService"
], function (directives) {
	directives.directive("nokiaMap", ["$window", "routingService", function ($window, routingService) {
		var nokia = $window.nokia;

		nokia.Settings.set("appId", "69Dgg78qt4obQKxVbRA8"); // TODO: move to settings
		nokia.Settings.set("app_code", "Nz7ilIB_v1CRwPXxgPdvuA");

		return {
			scope: {
				markers: "="
			},

			link: function ($scope, $element, $attrs) {
				var mapComponents = [
					new nokia.maps.map.component.InfoBubbles(),
					new nokia.maps.map.component.ZoomBar(),
					new nokia.maps.map.component.Behavior(),
					new nokia.maps.map.component.TypeSelector(),
					new nokia.maps.map.component.ScaleBar()
				];

				$scope.map = new nokia.maps.map.Display($element[0], {
					components: mapComponents,

					center: [52.51, 13.4],
					zoomLevel: 8,
				});

				$scope.$watchCollection("markers", function (markers) {
					drawTrace(markers);
					drawMarkers(markers);

				});

				$scope.markerContainer = new nokia.maps.map.Container();
				$scope.traceContainer = new nokia.maps.map.Container();

				$scope.map.objects.add($scope.markerContainer);
				$scope.map.objects.add($scope.traceContainer);

				$scope.$on("$destroy", function () {
					$scope.markerContainer.objects.clear();
					$scope.markerContainer.destroy();

					$scope.traceContainer.objects.clear();
					$scope.traceContainer.destroy();
				});

				function drawMarkers (markers) {
					$scope.markerContainer.objects.clear();

					if (markers && markers.length < 2) {
						markers.map(function (marker) {
							return new nokia.maps.geo.Coordinate(marker.latitude, marker.longitude);
						}).forEach(function (coord) {
							$scope.markerContainer.objects.add(new nokia.maps.map.StandardMarker(coord));
						});
					}
				}

				function drawTrace (markers) {
					$scope.traceContainer.objects.clear();

					if (markers && markers.length) {
						routingService.calculateRoute(markers).then(function (route) {
							$scope.traceContainer.objects.add(route);
							$scope.map.zoomTo(route.getBoundingBox(), false, "default");
						}, function (error) {

						});
					}
				}
			}
		};
	}]);
});