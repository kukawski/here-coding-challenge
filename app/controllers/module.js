define([
	"angular"
], function (ng) {
	return ng.module("challenge.controllers", []);
});