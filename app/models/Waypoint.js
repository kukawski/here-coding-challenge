define(function () {
	var Waypoint = function (obj) {
		this.latitude = (obj.position || obj).latitude;
		this.longitude = (obj.position || obj).longitude;
		this.title = obj.title;
	};

	return Waypoint;
});