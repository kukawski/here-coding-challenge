require.config({
	paths: {
		"angular": "libs/angular/angular.min",
		"ngRoute": "libs/angular-route/angular-route.min",
		"domReady": "libs/requirejs-domready/domReady",
		"jquery": "libs/jquery/jquery.min",
		"jquery-ui": "libs/jquery-ui/ui/minified/jquery-ui.min",
		"angular-ui-sortable": "libs/angular-ui-sortable/src/sortable",
		"angular-bootstrap": "libs/angular-bootstrap/ui-bootstrap.min"
	},

	shim: {
		"angular": {
			exports: "angular",
			deps: ["jquery", "jquery-ui"]
		},

		"ngRoute": {
			deps: ["angular"]
		},

		"angular-ui-sortable": {
			deps: ["angular", "jquery", "jquery-ui"]
		},

		"jquery-ui": {
			deps: ["jquery"],
			exports: "$"
		},

		"angular-bootstrap": {
			deps: ["jquery", "angular"]
		}
	}
});

require([
	"angular",
	"domReady!",
	"./app",
	"./routes",
	"angular-bootstrap"
], function (ng, doc) {
	ng.bootstrap(doc.documentElement, ["challenge"]);
});