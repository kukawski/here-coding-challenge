define([
	"./module",
	"../events",
	"../services/searchService"
], function (controllers, events) {
	"use strict";

	controllers.controller("RouteCreatorController", ["$scope", "searchService", function ($scope, searchService) {
		$scope.waypoints = [];

		$scope.searchTerms = "";
		$scope.searchResults = [];

		$scope.searchPlaces = function () {
			if ($scope.searchTerms) {
				searchService.search($scope.searchTerms).then(function (waypoints) {
					$scope.searchResults = waypoints;
				}, function (error) {
					$scope.searchResults = [];
				});
			}
		};

		$scope.selectSearchResult = function (waypoint) {
			$scope.waypoints.push(waypoint);
			$scope.searchResults = [];
			$scope.searchTerms = "";
		};

		$scope.removeWaypoint = function (waypoint) {
			var index = $scope.waypoints.indexOf(waypoint);

			if (index > -1) {
				$scope.waypoints.splice(index, 1);
			}
		};
	}]);
});