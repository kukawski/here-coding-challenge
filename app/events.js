define({
	ui: {
		search: {
			searchResultSelected: "ui-search-searchresult-selected"
		},

		waypoints: {
			waypointRemoved: "ui-waypoints-waypoint-removed",
			waypointsReordered: "ui-waypoints-reordered"
		}
	}
});