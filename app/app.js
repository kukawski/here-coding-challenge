define([
	"angular",
	"ngRoute",
	"angular-ui-sortable",
	"angular-bootstrap",
	"./controllers/index",
	"./directives/index",
	"./services/index"
], function (ng) {
	"use strict";

	var app = ng.module("challenge", [
		"ngRoute",
		"ui.sortable",
		"ui.bootstrap",
		"challenge.controllers",
		"challenge.directives",
		"challenge.services"
	]);

	return app;
});