define([
	"./module",
	"../models/Waypoint"
], function (services, Waypoint) {
	return services.factory("searchService", ["$q", function ($q) {
		var currentRequestId,
			currentRequestDeferred,
			errors = {
				cancelled: "Cancelled",
				requestFailed: "Request to HERE API failed"
			};

			function cancelPendingTask () {
				if (currentRequestId) {
					nokia.places.comm.data.abortRequest(currentRequestId);
					currentRequestDeferred.reject(errors.cancelled);
					currentRequestId = 0;
				}
			}

			function taskStateChange (responseData, status) {
				if (status === "OK") {
					currentRequestDeferred.resolve(responseData.results.items.map(function (item) {
						return new Waypoint(item);
					}));
				} else {
					currentRequestDeferred.reject(errors.requestFailed);
				}

				currentRequestId = 0;
			}

		return {
			errors: errors,
			search: function (searchQuery) {
				cancelPendingTask();

				currentRequestDeferred = $q.defer();

				currentRequestId = nokia.places.search.manager.findPlaces({
					searchTerm: searchQuery,
					onComplete: taskStateChange
				});

				return currentRequestDeferred.promise;
			}
		}
	}]);
});