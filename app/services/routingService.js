define([
	"./module"
], function (services) {
	return services.factory("routingService", ["$q", function ($q) {
		var routingDeferred;

		var modes = [{
			type: "shortest", 
			transportModes: ["car"],
			options: "avoidTollroad",
			trafficMode: "default"
		}];

		var router,
			deferred,
			errors = {
				cancelled: "Cancelled",
				notEnoughData: "Not enough waypoints to calculate route",
				requestFailed: "Request to HERE API failed"
			};

		function cancelPendingTask () {
			if (router && router.state === "started") {
				router.removeObserver("state", taskStateChange);
				router.clear();
				router.destroy();
				router = null;

				deferred.reject(errors.cancelled);
			}
		}

		function taskStateChange (observedRouter, key, value) {
			if (value == "finished") {
				var routes = observedRouter.getRoutes();
				
				var mapRoute = new nokia.maps.routing.component.RouteResultSet(routes[0]).container;
				deferred.resolve(mapRoute);
			} else if (value == "failed") {
				deferred.reject(errors.requestFailed);
			}
		}

		function startTask (waypoints, modes) {
			router = new nokia.maps.routing.Manager();

			router.addObserver("state", taskStateChange);
			router.calculateRoute(waypoints, modes);
		}

		return {
			errors: errors,
			calculateRoute: function (waypoints) {
				cancelPendingTask();

				deferred = $q.defer();

				var waypointsList = new nokia.maps.routing.WaypointParameterList();

				waypoints.forEach(function (waypoint) {
					waypointsList.addCoordinate(new nokia.maps.geo.Coordinate(waypoint.latitude, waypoint.longitude));
				});

				if (waypointsList.size() < 2) {
					deferred.reject(errors.notEnoghData);
				} else {
					startTask(waypointsList, modes);
				}

				return deferred.promise;
			}
		};
	}]);
})